﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Hangman_WPF
{
    /// <summary>
    /// Interaction logic for NewDialog.xaml
    /// </summary>
    public partial class NewDialog : Window
    {
        DispatcherTimer timer = new DispatcherTimer();
        Button clicked;

        public int Number
        {
            get;
            private set;
        }
        
        public NewDialog(string title, string labeltext, string button1text, string button2text)
        {
            InitializeComponent();

            titleLabel.Text = title;
            textLabel.Text = labeltext;

            if (button1text == "Disable")
            {
                button1.Visibility = Visibility.Hidden;
                
                Grid.SetRowSpan(button2, 2);

                button2.HorizontalAlignment = HorizontalAlignment.Center;
            }
            else
                button1.Content = button1text;

            if (button2text == "Disable")
            {
                button2.Visibility = Visibility.Hidden;
                Grid.SetRowSpan(button1, 2);

                button1.HorizontalAlignment = HorizontalAlignment.Center;
            }
            else
                button2.Content = button2text;

        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;

            Blink(sender);

        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;

            Blink(sender);

        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key.ToString().ToUpper() == "N")
                button2_Click(button2, new RoutedEventArgs());
            else if (e.Key.ToString().ToUpper() == "Y")
                button1_Click(button1, new RoutedEventArgs());
        }

        private void Blink(object b)
        {

            if (timer.IsEnabled)
                timer_Tick(timer, new EventArgs());

            clicked = (Button)b;

            timer.Interval = new TimeSpan((long)1000000);
            timer.Tick += timer_Tick;

            clicked.Background = Brushes.Maroon;
            clicked.Foreground = Brushes.Moccasin;

            timer.Start();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            clicked.Foreground = Brushes.Maroon;
            clicked.Background = Brushes.Moccasin;

            (sender as DispatcherTimer).Stop();

            this.Close();
        }
    }
}
