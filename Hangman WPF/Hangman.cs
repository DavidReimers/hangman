﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Hangman
{
    class Hangman
    {
        List<string> wordList = new List<string>();     //Will be used to contain a list of words out of which one will be randomly selected for each game. 
        Random r = new Random();
        
        public Hangman()
        {
            // The point of the try-catch block is mainly to catch any file reading error in the InitiateWords method.

            try
            {
                // The method InitiateWords populates the List wordList with a number of words to use for the game.
                InitiateWords();

                //Print a greeting message to the user
                Console.WriteLine("*** Welcome to Hangman ***");
                Console.WriteLine("__________________________\n");

                string input;

                do
                {
                    // The method StartGame contains the main game logic.
                    StartGame();

                    // After the game is finished, the user can choose to play again or quit. 
                    
                    Console.Write("Do you want to play again (Y/N)? ");
                    
                    input = Console.ReadLine();
                    Console.Clear();

                } while (input.ToUpper() == "Y");       // Any other input than "Y" or "y" will quit the game. 

            }
            catch // Any exception (most likely file reading error from the InitiateWords method) will exit the application.
            {
                Console.WriteLine("File error. Application ending");
                Console.ReadLine();
            }

        }

        public void InitiateWords()
        {
            // wordList is populated by reading from a textfile. The textfile contains all (or nearly all) countries in the world, one country on each line.
            
            using (StreamReader reader = new StreamReader("CountryList.txt"))
            {
                while (!reader.EndOfStream)
                {
                    wordList.Add(reader.ReadLine().ToUpper());
                }
            }
        }

        public void StartGame()
        {
            string word = wordList[r.Next(wordList.Count)]; // One word is randomly selected from the wordList.
            
            int hangStatus = 0;                             // Will be used to track the number of wrong guesses.
            
            List<char> used = new List<char>();             // Will be used to contain the guessed letters.
            
            bool[] found = new bool[word.Length];           // This bool array keeps track of which letters in the word that have been found

            for (int i = 0; i < word.Length; i++)           // If there are any spaces in the word, those positions are set as found from the start.
                if (word[i] == ' ')
                    found[i] = true;

            bool completed = false;                         // This bool keep track of whether the entire word has been 

            // This do-while loop contains the main game process
            do
            {
                // The method printHangman prints the current status of the hanged man.
                printHangman(hangStatus);

                // If the user has guessed wrong 7 times, the man is hanged and the game is over.
                if (hangStatus == 7)
                {
                    Console.WriteLine("You have lost, the man has been hanged.\n");
                    Console.WriteLine("The word was {0}.\n", word);
                    return;
                }

                // The word is printed, all not found letters are printed as '_'.
                for (int i = 0; i < word.Length; i++)
                {
                    Console.Write(" {0}", found[i] ? word[i].ToString() : "_");
                }

                Console.WriteLine("\n\n");

                // All used letters are printed.
                Console.Write("Used letters: ");
                
                foreach (char c in used)
                    Console.Write("{0}, ", c);
                
                Console.WriteLine("\n");
                
                string input;
                char inputchar;
                
                // This do-while loop collects the next guess from the user. The user has to enter a single letter that has not been used before, otherwise he/she has to try again.
                do
                {
                    Console.Write("Please enter your next guess: ");
                    input = Console.ReadLine();
                    Console.WriteLine();

                } while (input.Length != 1 || !char.IsLetter(inputchar = input.ToUpper().ToCharArray()[0]) || used.Contains(inputchar)); 

                // The new guess is added to the used letters list.

                used.Add(inputchar); 
                Console.Clear();

                if (word.Contains(inputchar)) // Does the word contain the guessed letter?
                {
                    completed = true;

                    // This for loop runs through the entire word and sets the found bool Array to true for all instances of the guessed letter.
                    // If all letters are found, the bool completed will be true.
                    
                    for (int i = 0; i < word.Length; i++)
                    {
                        if (word[i] == inputchar)
                            found[i] = true;

                        if (!found[i])
                            completed = false;
                    }

                    // If completed is true, the user has found the entire word and the game is over.
                    
                    if (completed)
                    {
                        Console.WriteLine("You won, you found the complete word!\n");
                        Console.WriteLine("The word is {0}.\n", word);
                        return;
                    }

                    // If the game is not completed, the user gets a status update.
                    
                    Console.WriteLine("The word contains the letter '{0}'!\n", inputchar);
                }
                else // If the guess is incorrect, the user gets a status update and the number of false guesses is increased by 1. The loop then starts over.
                {
                    Console.WriteLine("The word does not contain the letter '{0}'!\n", inputchar);
                    hangStatus++;
                }

            } while (true);

        }

        // The printHangman method prints the current status of the hanged man, based on the int status.
        // This is just a number of if-else statements that prints the desired picture based on status.
        // Note that to print backslash, the escape sequence \\ is needed.
        public void printHangman(int status)
        {
            if (status >= 3)
            {
                Console.WriteLine("    ________");
                Console.WriteLine("   /        \\");
                Console.WriteLine("   |        |");
            }
            else if (status >= 2)
            {
                Console.WriteLine("   |");
            }

            if (status >= 4)
                Console.WriteLine("   |        O");
            else if (status >= 2)
                Console.WriteLine("   |");

            if (status >= 6)
                Console.WriteLine("   |       /|\\");
            else if (status >= 5)
                Console.WriteLine("   |        |");
            else if (status >= 2)
                Console.WriteLine("   |");

            if (status >= 5)
                Console.WriteLine("   |        |");
            else if (status >= 2)
                Console.WriteLine("   |");

            if (status == 7)
                Console.WriteLine("   |       / \\");
            else if (status >= 2)
                Console.WriteLine("   |");

            if (status >= 1)
            {
                Console.WriteLine("  / \\");
                Console.WriteLine(" /   \\");
                Console.WriteLine("/_____\\__________");
            }

            Console.WriteLine();
        }
    }
}
