﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Hangman_WPF
{
    /// <summary>
    /// Interaction logic for StartScreen.xaml
    /// </summary>
    public partial class StartScreen : Window
    {
        private DispatcherTimer timer = new DispatcherTimer();
        private Button clicked;

        public StartScreen()
        {
            InitializeComponent();
        }

        public string FileName { get; private set; }

        private void Countries_Click(object sender, RoutedEventArgs e)
        {
            Blink(sender);
            FileName = Properties.Resources.CountryList;
            
        }

        private void Cities_Click(object sender, RoutedEventArgs e)
        {
            Blink(sender);
            FileName = Properties.Resources.CityList;
            
        }

        private void Animals_Click(object sender, RoutedEventArgs e)
        {
            Blink(sender);
            FileName = Properties.Resources.AnimalList;
            
        }

        private void States_Click(object sender, RoutedEventArgs e)
        {
            Blink(sender);
            FileName = Properties.Resources.StateList;
            
        }

        private void Nobel_Click(object sender, RoutedEventArgs e)
        {
            Blink(sender);
            FileName = Properties.Resources.NobelList;
            
        }

        private void Stars_Click(object sender, RoutedEventArgs e)
        {
            Blink(sender);
            FileName = Properties.Resources.StarList;
            
        }

        private void Movies_Click(object sender, RoutedEventArgs e)
        {
            Blink(sender);
            FileName = Properties.Resources.MovieList;
           
        }

        private void Beers_Click(object sender, RoutedEventArgs e)
        {
            Blink(sender);
            FileName = Properties.Resources.BeerList;
            
        }

        private void Blink(object b)
        {

            if (timer.IsEnabled)
                timer_Tick(timer, new EventArgs());

            clicked = (Button)b;

            timer.Interval = new TimeSpan((long)1000000);
            timer.Tick += timer_Tick;

            clicked.Background = Brushes.Maroon;
            clicked.Foreground = Brushes.Moccasin;

            timer.Start();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            clicked.Foreground = Brushes.Maroon;
            clicked.Background = Brushes.Moccasin;

            (sender as DispatcherTimer).Stop();

            this.Close();
        }
    }
}
